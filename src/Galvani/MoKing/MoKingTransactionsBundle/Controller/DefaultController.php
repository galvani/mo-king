<?php

namespace Galvani\MoKing\MoKingTransactionsBundle\Controller;

use Galvani\MoKing\Form\TransactionType;
use Galvani\MoKing\MoKingBundle\Document\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="transactions")
     */
    public function indexAction()
    {
    	$transactions = $this->get('doctrine_mongodb')->getRepository('MoKingBundle:Transaction')->findByUser(
    		$this->getUser()
		);

        return $this->render('MoKingTransactionsBundle:Default:index.html.twig',[
        	'transactions'	=> $transactions
		]);
    }

	/**
	 * @Route("/new", name="transaction_new")
	 */
	public function newAction(Request $request)
	{
		$transaction = new Transaction();
		$transaction
			->setUser($this->getUser())
			->setDate(new \DateTime);


		$form = $this->createForm(TransactionType::class, $transaction);

		return $this->render('MoKingTransactionsBundle:Transaction:new.html.twig',array(
			'form'	=> $form->createView()
		));
	}
}
