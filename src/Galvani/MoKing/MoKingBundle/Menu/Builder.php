<?php

namespace Galvani\MoKing\MoKingBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\Matcher\Matcher;
use Knp\Menu\Matcher\Voter\UriVoter;
use Knp\Menu\Renderer\ListRenderer;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface {
	use ContainerAwareTrait;

	public function mainMenu(FactoryInterface $factory, array $options) {
		$menu = $factory->createItem('MoKing', array(
			'currentClass' => 'active',
			'childrenAttributes' => array(
				'class' => 'nav navbar-nav'
			),
		));


		$menu->addChild('home', array('route' => 'homepage', 'label'=>'Dashboard'));
		$transactions = $menu->addChild('transactions', array('label'=>'Transactions  <span class="caret"></span>','route' => 'transactions', 'extras' => array('safe_label' => true)));
		//$transactions->setExtra('safe_label', true);
		$transactions->setAttribute('class', 'dropdown');
		$transactions->setLinkAttribute('class','dropdown-toggle');
		$transactions->setLinkAttribute('data-toggle', 'dropdown');
		$transactions->setChildrenAttribute('class','dropdown-menu');
		$transactions->addChild('Transactions', array('route' => 'transactions'));
		$transactions->addChild('Add Transaction', array('route' => 'transaction_new'));


		$matcher = new Matcher();
		$matcher->addVoter(new UriVoter($_SERVER['REQUEST_URI']));

		$renderer = new ListRenderer($matcher);

		//$menu->
//		$menu->addChild('Latest Blog Post', array(
//			'route'           => 'blog_show',
//			'routeParameters' => array('id' => 1)
//		));

//		// create another menu item
//		$menu->addChild('About Me', array('route' => 'about'));
//		// you can also add sub level's to your menu's as follows
//		$menu['About Me']->addChild('Edit profile', array('route' => 'edit_profile'));

		$menu->setAttribute('currentClass','active');

		//var_dump($menu); die();

		$matcher = new Matcher();
		$matcher->addVoter(new UriVoter($_SERVER['REQUEST_URI']));


		return $menu;
	}
}