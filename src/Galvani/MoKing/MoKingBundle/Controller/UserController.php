<?php

namespace Galvani\MoKing\MoKingBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Galvani\MoKing\MoKingBundle\Document\Transaction;
use Galvani\MoKing\MoKingBundle\Document\User;
use Galvani\MoKing\MoKingBundle\Form\TransactionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/user")
 */
class UserController extends Controller {
	/**
	 * @Method("GET")
	 * @Route("/tags.{_format}", name="user_tags")
	 *     name="user_tags",
	 *     defaults={"_format": "json"},
	 *     requirements={
	 *         "_format": "html|json|xml"
	 *     }
	 * )
	 */
	public function tagsAction() {
		$tags = $this->getUser()->getTags();
		$tags = is_null($tags) ? ['Paris','Prague'] : $tags;

		return new JsonResponse($tags);
	}

}
