<?php

namespace Galvani\MoKing\MoKingBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Galvani\MoKing\MoKingBundle\Document\Transaction;
use Galvani\MoKing\MoKingBundle\Document\User;
use Galvani\MoKing\MoKingBundle\Form\TransactionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/transactions")
 */
class TransactionController extends Controller {
	/**
	 * @Route("/", name="transactions")
	 */
	public function indexAction() {
		$transactions = $this->get('doctrine_mongodb')->getRepository('MoKingBundle:Transaction')->findByUser(
			$this->getUser()
		);

		return $this->render('MoKingBundle:Transaction:index.html.twig', [
			'transactions' => $transactions,
			'toolbar' => $this->getToolbar()
		]);
	}

	/**
	 * @Method("GET")
	 * @Route("/new", name="transaction_new")
	 */
	public function newAction(Request $request) {
		/** @var User $user */
		$user = $this->getUser();

		/** @var DocumentManager $dm */
		$dm = $this->get('doctrine_mongodb')->getManager();


		$lastTransaction = $this->get('doctrine_mongodb')->getRepository('MoKingBundle:Transaction')->findOneBy(
			['user' => $this->getUser()],
			['date' => 'DESC']
		);

		$currency = $lastTransaction ? $lastTransaction->getCurrency() : 'EUR';

		$transaction = new Transaction();
		$transaction
			->setUser($this->getUser())
			->setCurrency($currency)
			->setDate(new \DateTime);


		$form = $this->createForm(TransactionType::class, $transaction, array(
			'action'	=> $this->generateUrl('transaction_add'),
			'method'	=> 'POST',
			'attr'		=> [
				'currency'	=> $currency
			]
		));

		return $this->render('MoKingBundle:Transaction:new.html.twig', array(
			'form'    => $form->createView(),
			'toolbar' => $this->getToolbar()
		));
	}



	/**
	 * @Method("POST")
	 * @Route("/add", name="transaction_add")
	 */
	public function addAction(Request $request) {
		$transaction = new Transaction();

		$transaction
			->setUser($this->getUser())
			->setCreated(new \DateTime);

		$form = $this->createForm(TransactionType::class, $transaction, array(
			'action' => $this->generateUrl('transaction_add'),
			'method' => 'POST'
		));

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();

			$dm = $this->get('doctrine_mongodb')->getManager();
			$dm->persist($transaction);
			$dm->flush();

			return new RedirectResponse($this->generateUrl('transaction_new'));
		}

		return $this->render('MoKingBundle:Transaction:new.html.twig', array(
			'form'    => $form->createView(),
			'toolbar' => $this->getToolbar()
		));
	}

	/**
	 * @Method("GET|POST")
	 * @Route("/detail/{id}.{_format}",
	 *     name="transaction_detail",
	 *     defaults={"_format": "html"},
	 *     requirements={
	 *         "_format": "html|json|xml",
	 *         "id": ".{24}"
	 *     }
	 * )
	 */
	public function detailAction($id, $_format) {
		/** @var DocumentManager $dm */
		$dm = $this->get('doctrine_mongodb')->getManager();

		$transaction = $dm->getRepository('MoKingBundle:Transaction')->find($id);

		$form = $this->createForm(TransactionType::class, $transaction, array(
			'action'	=> $this->generateUrl('transaction_detail', [
				'id'=> $transaction->getId()
			]),
			'method'	=> 'POST'
		));

		$request = Request::createFromGlobals();

		if ($request->isMethod(Request::METHOD_POST)) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$data = $form->getData();

				$dm = $this->get('doctrine_mongodb')->getManager();
				$dm->persist($transaction);
				$dm->flush();

				return new RedirectResponse($this->generateUrl('transactions'));
			}
		}

		return $this->render('MoKingBundle:Transaction:new.html.twig', array(
			'form'    => $form->createView(),
			'toolbar' => $this->getToolbar()
		));
	}

	protected function getToolbar() {
		$lastTransaction = $this->get('doctrine_mongodb')->getRepository('MoKingBundle:Transaction')->findOneBy(
			['user' => $this->getUser()],
			['date' => 'DESC']
		);

		$currency = $lastTransaction ? $lastTransaction->getCurrency() : 'EUR';
		return (string) $this->renderView('MoKingBundle:Transaction:submenu.html.twig',[
			'currency'	=> $currency
		]);
	}
}
