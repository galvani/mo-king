<?php
/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  23.6.17
 */

namespace Galvani\MoKing\MoKingBundle\Form;

use Galvani\MoKing\MoKingBundle\Document\Account;
use Galvani\MoKing\MoKingBundle\Document\Transaction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PartnerType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('alias',TextType::class)
			->add('value', MoneyType::class, array(
				'required'	=> true
			))
			->add('currency', CurrencyType::class)
			->add('account', null, array(
				'required'	=> false
			))
			->add('partner')
			->add('save', SubmitType::class)
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => Transaction::class,
		));
	}
}