<?php
/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  23.6.17
 */

namespace Galvani\MoKing\MoKingBundle\Form;

use Galvani\MoKing\MoKingBundle\Document\Account;
use Galvani\MoKing\MoKingBundle\Document\Transaction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TransactionType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('date',DateType::class, array(
				'attr' => array('class' => 'date_field'),
				'format' => 'yyyy-MM-dd',
				'data'	=> new \DateTime
			))
			->add('currency', CurrencyType::class, array(
				'label'	=> false,
				'attr' => [
					'class' => 'input-group-addon'
				]
			))
			->add('value', MoneyType::class, array(
				'required'	=> true,
				//'currency'	=> $options['attr']['currency'],
				'currency'	=> false,
				'scale'		=> 0,
				'attr'	=> [
					'class' => ''
				]
			))
			->add('subject', null, array(
				'required'	=> false
			))
			->add('account', null, array(
				'required'	=> false
			))
			->add('tags', TextType::class, array(
				'required'	=> false,
				'attr'	=> [
					'class' => 'input-tags',
					'data-role' => 'tagsinput'
				]
			))
			->add('partner', null, array(
				'required'	=> false
			))
			->add('location', LocationType::class)
			->add('save', SubmitType::class)
		;

		$builder->get('tags')->addModelTransformer(new CallbackTransformer(
			function ($tagsAsArray) {
				return is_null($tagsAsArray) ? [] : implode(', ', $tagsAsArray);
			},
			function ($tagsAsString) {
				return explode(',', $tagsAsString);
			}
		));

		$builder->get('location')->addModelTransformer(new CallbackTransformer(
			function ($locationJSON) {
				return json_decode($locationJSON);
				return is_null($locationJSON) ? "{}" : json_encode($locationJSON);
			},
			function ($locationJSON) {
				return json_decode($locationJSON);
			}
		));

		$builder->setAttributes(array(
			'method' => 'POST'
		));
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => Transaction::class
		));
	}
}