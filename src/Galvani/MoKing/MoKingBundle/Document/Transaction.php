<?php

namespace Galvani\MoKing\MoKingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Galvani\MoKing\MoKingBundle\Document\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  9.6.17
 *
 * @MongoDB\Document(repositoryClass="Galvani\MoKing\MoKingBundle\Repository\TransactionRepository")
 */
class Transaction {
	/**
	 * @MongoDB\Id
	 */
	protected $id;

	/**
	 * @var User
	 *
	 * @MongoDB\ReferenceOne(targetDocument="User")
	 */
	protected $user;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Type("\DateTime")
	 * @MongoDB\Field(type="date")
	 */
	protected $created;

	/**
	 * @Assert\Type("\DateTime")
	 * @MongoDB\Field(type="date")
	 */
	protected $date;

	/**
	 * @Assert\NotBlank()
	 * @MongoDB\Field(type="float")
	 */
	protected $value;

	/**
	 * @MongoDB\Field(type="string")
	 */
	protected $currency;

	/**
	 * @MongoDB\ReferenceOne(targetDocument="Account")
	 */
	protected $account;

	/**
	 * @MongoDB\ReferenceOne(targetDocument="Partner")
	 */
	protected $partner;

	/**
	 * @Assert\NotBlank()
	 * @MongoDB\Field(type="string")
	 */
	protected $subject;

	/**
	 * @MongoDB\Field(type="hash")
	 */
	protected $tags;

	/**
	 * @MongoDB\Field(type="hash")
	 */
	protected $location;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param Galvani\MoKing\MoKingBundle\Document\User $user
     * @return $this
     */
    public function setUser(\Galvani\MoKing\MoKingBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Galvani\MoKing\MoKingBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set created
     *
     * @param date $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return date $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return float $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get currency
     *
     * @return string $currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set account
     *
     * @param Galvani\MoKing\MoKingBundle\Document\Account $account
     * @return $this
     */
    public function setAccount(\Galvani\MoKing\MoKingBundle\Document\Account $account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * Get account
     *
     * @return Galvani\MoKing\MoKingBundle\Document\Account $account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set partner
     *
     * @param Galvani\MoKing\MoKingBundle\Document\Partner $partner
     * @return $this
     */
    public function setPartner(\Galvani\MoKing\MoKingBundle\Document\Partner $partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * Get partner
     *
     * @return Galvani\MoKing\MoKingBundle\Document\Partner $partner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Get subject
     *
     * @return string $subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set tags
     *
     * @param hash $tags
     * @return $this
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Get tags
     *
     * @return hash $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set location
     *
     * @param hash $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get location
     *
     * @return hash $location
     */
    public function getLocation()
    {
        return $this->location;
    }
}
