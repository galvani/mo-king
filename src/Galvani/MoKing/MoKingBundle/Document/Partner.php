<?php

namespace Galvani\MoKing\MoKingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Galvani\MoKing\MoKingBundle\Document\User;

/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  9.6.17
 *
 * @MongoDB\Document(repositoryClass="Galvani\MoKing\MoKingBundle\Repository\PartnerRepository")
 */
class Partner {
	/**
	 * @MongoDB\Id
	 */
	protected $id;

	/**
	 * @MongoDB\ReferenceOne(targetDocument="User")
	 */
	protected $user;

	/**
	 * @MongoDB\Field(type="string")
	 */
	protected $alias;

	/**
	 * @MongoDB\Field(type="float")
	 */
	protected $value;

	/**
	 * @MongoDB\Field(type="string")
	 */
	protected $currency;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param Galvani\MoKing\MoKingBundle\Document\User $user
     * @return $this
     */
    public function setUser(\Galvani\MoKing\MoKingBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Galvani\MoKing\MoKingBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * Get alias
     *
     * @return string $alias
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return float $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get currency
     *
     * @return string $currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
