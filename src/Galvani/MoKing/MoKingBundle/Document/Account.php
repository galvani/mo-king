<?php

namespace Galvani\MoKing\MoKingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Galvani\MoKing\MoKingBundle\Document\User;

/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  9.6.17
 *
 * @MongoDB\Document(repositoryClass="Galvani\MoKing\MoKingBundle\Repository\AccountRepository")
 */
class Account {
	/**
	 * @MongoDB\Id
	 */
	protected $id;

	/**
	 * @var User
	 *
	 * @MongoDB\ReferenceOne(targetDocument="User")
	 */
	protected $user;

	/**
	 * @MongoDB\Field(type="float")
	 */
	protected $date;

	/**
	 * @MongoDB\Field(type="float")
	 */
	protected $value;

	/**
	 * @var Transaction
	 *
	 * @MongoDB\ReferenceMany(targetDocument="Transaction")
	 */
	protected $transactions;

    public function __construct()
    {
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set date
     *
     * @param float $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return float $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return float $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add transaction
     *
     * @param Galvani\MoKing\MoKingBundle\Document\Transaction $transaction
     */
    public function addTransaction(\Galvani\MoKing\MoKingBundle\Document\Transaction $transaction)
    {
        $this->transactions[] = $transaction;
    }

    /**
     * Remove transaction
     *
     * @param Galvani\MoKing\MoKingBundle\Document\Transaction $transaction
     */
    public function removeTransaction(\Galvani\MoKing\MoKingBundle\Document\Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection $transactions
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}
