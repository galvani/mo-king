<?php

namespace Galvani\MoKing\MoKingBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  9.6.17
 *
 * @MongoDB\Document(repositoryClass="Galvani\MoKing\MoKingBundle\Repository\UserRepository")
 */
class User extends BaseUser {
	/**
	 * @MongoDB\Id(strategy="auto")
	 */
	protected $id;

	/**
	 * @MongoDB\Field(type="string")
	 */
	protected $username;

	/**
	 * @MongoDB\Field(type="boolean")
	 */
	protected $enabled;

	/**
	 * @MongoDB\Field(type="hash")
	 */
	protected $locale;

	/**
	 * @MongoDB\Field(type="hash")
	 */
	protected $profile;

	/**
	 * @MongoDB\Field(type="hash")
	 */
	protected $tags;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string $username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean $enabled
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set profile
     *
     * @param hash $profile
     * @return $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * Get profile
     *
     * @return hash $profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set locale
     *
     * @param hash $locale
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Get locale
     *
     * @return hash $locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set tags
     *
     * @param hash $tags
     * @return $this
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Get tags
     *
     * @return hash $tags
     */
    public function getTags()
    {
        return $this->tags;
    }
}
