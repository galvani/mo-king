var moking = {
  registerEvent: function (element) {
    var deepestFocus = 0;
    var focusElement = false;
    $("input.autofocus").each(function (id, el) {
      if ($(el).parents().length > deepestFocus) {
        focusElement = el;
      }
    });
    if (focusElement) {
      $(focusElement).focus();
    }

    $("input[type='date']", element).datetimepicker();
  },
  ttAdapter: function () {
    console.error('ttAdaper');
  }
}

$(function () {
  moking.registerEvent($('body'));
  var elt = $('#transaction_tags').tagsinput('input');

  var tags = new Bloodhound({
    initialize: true,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: {
      url: '/user/tags.json',
      cache: false,
      filter: function (list) {
        return $.map(list, function (tag) {
          return {name: tag};
        });
      }
    }
  });

  var partners = new Bloodhound({
    initialize: true,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: {
      url: '/user/partners.json',
      cache: false,
      filter: function (list) {
        return $.map(list, function (tag) {
          return {name: tag};
        });
      }
    }
  });

  elt.tagsinput({
    typeaheadjs: {
      name:       'tags2',
      displayKey: 'name',
      valueKey:   'name',
      source:     tags.ttAdapter()
    }
  });
});