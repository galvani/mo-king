<?php
/**
 * @author jan kozak <galvani78@gmail.com>
 * @since  24.6.17
 */

namespace Galvani\MoKing\MoKingBundle\Library\Deployment;

use Symfony\Component\Process\Process;
use Mage\Task\Exception\ErrorException;
use Mage\Task\AbstractTask;

class FixPermissions extends AbstractTask
{
	public function getName() {
		return 'galvani/fix-permissions';
	}

	public function getDescription() {
		return '[Galvani] Fixing Permissions';
	}

	public function execute()
	{
		if (!array_key_exists('server', $this->options) || !array_key_exists('port', $this->options)) {
			throw new ErrorException('Parameters "server" and "port" are required.');
		}

		$cmd = sprintf('echo "flush_all" | netcat %s %d', $this->options['server'], $this->options['port']);

		/** @var Process $process */
		$process = $this->runtime->runCommand($cmd);
		return $process->isSuccessful();
	}

	public function run()
	{
		$command = 'chmod 755 . -R';
		$result = $this->runCommandRemote($command);

		if ( true == $result ) {
			$result = $this->fixCachePermissions();
		}

		if ( true == $result ) {
			$result = $this->fixLogsPermissions();
		}

		return $result;
	}


	public function fixCachePermissions() {
		$command = 'chmod 775 ./app/cache -R';
		$result = $this->runCommandRemote($command);

		return $result;
	}


	public function fixLogsPermissions() {
		$command = 'chmod 775 ./app/logs -R';
		$result = $this->runCommandRemote($command);

		return $result;
	}
}